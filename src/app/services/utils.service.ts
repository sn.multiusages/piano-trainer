import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export class Chord {
  name: string;
  type: string;
  pf: string[];
}

export class Translation {
  word: string;
  translation: string;
}


@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  private subjectDisplayChord = new Subject<any>();
  private subjectClearKeyboard = new Subject<any>();

  constructor() { }

  Chords: Chord[] = [
    // MAJOR
    { name: 'c', type: 'major', pf: ['c1', 'e1', 'g1'] },
    { name: 'd', type: 'major', pf: ['d1', 'fs1', 'a1'] },
    { name: 'e', type: 'major', pf: ['e1', 'gs1', 'b1'] },
    { name: 'f', type: 'major', pf: ['f1', 'a1', 'c2'] },
    { name: 'g', type: 'major', pf: ['g1', 'b1', 'd2'] },
    { name: 'a', type: 'major', pf: ['a1', 'cs2', 'e2'] },
    { name: 'b', type: 'major', pf: ['b1', 'ds2', 'fs2'] },
    // # MAJOR
    { name: 'c#', type: 'sharp-major', pf: ['cs1', 'f1', 'gs1'] },
    { name: 'd#', type: 'sharp-major', pf: ['ds1', 'g1', 'as1'] },
    { name: 'f#', type: 'sharp-major', pf: ['fs1', 'as1', 'cs2'] },
    { name: 'g#', type: 'sharp-major', pf: ['gs1', 'c2', 'ds2'] },
    { name: 'a#', type: 'sharp-major', pf: ['as1', 'd2', 'f2'] },
    // MINOR
    { name: 'cm', type: 'minor', pf: ['c1', 'ds1', 'g1'] },
    { name: 'dm', type: 'minor', pf: ['d1', 'f1', 'a1'] },
    { name: 'em', type: 'minor', pf: ['e1', 'g1', 'b1'] },
    { name: 'fm', type: 'minor', pf: ['f1', 'gs1', 'c2'] },
    { name: 'gm', type: 'minor', pf: ['g1', 'as1', 'd2'] },
    { name: 'am', type: 'minor', pf: ['a1', 'c2', 'e2'] },
    { name: 'bm', type: 'minor', pf: ['b1', 'd2', 'fs2'] },
    // # MINOR
    { name: 'c#m', type: 'sharp-minor', pf: ['cs1', 'e1', 'gs1'] },
    { name: 'd#m', type: 'sharp-minor', pf: ['ds1', 'fs1', 'as1'] },
    { name: 'f#m', type: 'sharp-minor', pf: ['fs1', 'a1', 'cs2'] },
    { name: 'g#m', type: 'sharp-minor', pf: ['gs1', 'b1', 'ds2'] },
    { name: 'a#m', type: 'sharp-minor', pf: ['as1', 'cs2', 'f2'] },
  ];
  
  FrenchDico: Translation[] = [
    {word: '', translation: ''},
    {word: '--', translation: '--'},
    {word: '...', translation: '...'},
    // MAJOR
    {word: 'c', translation: 'DO'},
    {word: 'd', translation: 'RE'},
    {word: 'e', translation: 'MI'},
    {word: 'f', translation: 'FA'},
    {word: 'g', translation: 'SOL'},
    {word: 'a', translation: 'LA'},
    {word: 'b', translation: 'SI'},
    {word: 'c#', translation: 'DO#'},
    {word: 'd#', translation: 'RE#'},
    {word: 'f#', translation: 'FA#'},
    {word: 'g#', translation: 'SOL#'},
    {word: 'a#', translation: 'LA#'},
    // MINOR
    {word: 'cm', translation: 'DOm'},
    {word: 'dm', translation: 'REm'},
    {word: 'em', translation: 'MIm'},
    {word: 'fm', translation: 'FAm'},
    {word: 'gm', translation: 'SOLm'},
    {word: 'am', translation: 'LAm'},
    {word: 'bm', translation: 'SIm'},
    {word: 'c#m', translation: 'DO#m'},
    {word: 'd#m', translation: 'RE#m'},
    {word: 'f#m', translation: 'FA#m'},
    {word: 'g#', translation: 'SOL#m'},
    {word: 'a#m', translation: 'LA#m'},
  ];

  clearKeayboard() {
    this.subjectClearKeyboard.next();
  }

  getClearKeayboardEvt(): Observable<any>{ 
    return this.subjectClearKeyboard.asObservable();
  }

  displayChord(chordName) {
    this.subjectDisplayChord.next(chordName);
  }

  getDisplayChordEvt(): Observable<any>{ 
    return this.subjectDisplayChord.asObservable();
  }

  getChordNameList() {
    let list: string[] = [];
    for (var chord of this.Chords) {
      list.push(chord.name);
    }
    return (list)
  }

  getChordKeysPf(chordName) {
    return (this.Chords.find(i => i.name === chordName).pf)
  }

  getRandomChord() {
    return this.Chords[Math.floor(Math.random() * this.Chords.length)];
  }

  toFrenchNotation(note) {
    return(this.FrenchDico.find(i => i.word === note.toLowerCase()).translation)
  }
}
