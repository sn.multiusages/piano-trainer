import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainerComponent } from './components/trainer/trainer.component';
import { ChordDirectoryComponent } from './components/chord-directory/chord-directory.component';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'chord-directory', component: ChordDirectoryComponent },
  { path: 'training', component: TrainerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
