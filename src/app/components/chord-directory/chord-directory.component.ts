import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-chord-directory',
  templateUrl: './chord-directory.component.html',
  styleUrls: ['./chord-directory.component.scss']
})
export class ChordDirectoryComponent implements OnInit {

  constructor(private utils: UtilsService) { }

  ngOnInit(): void {
  }

  onClickChordBtn(chord) {
    this.utils.displayChord(chord);
  }

}
