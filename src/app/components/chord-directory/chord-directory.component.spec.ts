import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChordDirectoryComponent } from './chord-directory.component';

describe('ChordDirectoryComponent', () => {
  let component: ChordDirectoryComponent;
  let fixture: ComponentFixture<ChordDirectoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChordDirectoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChordDirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
