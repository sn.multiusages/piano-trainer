import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils.service';
import { Subscription } from 'rxjs';

export class Key {
  name: string;
  color: string;
  note: string;
  highlighted: boolean;
}


@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent implements OnInit {

  Keyboard: Key[] = [
    { name: 'c1',   color: 'w', note: 'c',  highlighted: false },
    { name: 'cs1',  color: 'b', note: 'c#', highlighted: false },
    { name: 'd1',   color: 'w', note: 'd',  highlighted: false },
    { name: 'ds1',  color: 'b', note: 'd#', highlighted: false },
    { name: 'e1',   color: 'w', note: 'e',  highlighted: false },
    { name: 'f1',   color: 'w', note: 'f',  highlighted: false },
    { name: 'fs1',  color: 'b', note: 'f#', highlighted: false },
    { name: 'g1',   color: 'w', note: 'g',  highlighted: false },
    { name: 'gs1',  color: 'b', note: 'g#', highlighted: false },
    { name: 'a1',   color: 'w', note: 'a',  highlighted: false },
    { name: 'as1',  color: 'b', note: 'a#', highlighted: false },
    { name: 'b1',   color: 'w', note: 'b',  highlighted: false },
    { name: 'c2',   color: 'w', note: 'c',  highlighted: false },
    { name: 'cs2',  color: 'b', note: 'c#', highlighted: false },
    { name: 'd2',   color: 'w', note: 'd',  highlighted: false },
    { name: 'ds2',  color: 'b', note: 'd#', highlighted: false },
    { name: 'e2',   color: 'w', note: 'e',  highlighted: false },
    { name: 'f2',   color: 'w', note: 'f',  highlighted: false },
    { name: 'fs2',  color: 'b', note: 'f#', highlighted: false },
    { name: 'g2',   color: 'w', note: 'g',  highlighted: false },
    { name: 'gs2',  color: 'b', note: 'g#', highlighted: false },
    { name: 'a2',   color: 'w', note: 'a',  highlighted: false },
    { name: 'as2',  color: 'b', note: 'a#', highlighted: false },
    { name: 'b2',   color: 'w', note: 'b',  highlighted: false },
  ];

  


  displayChordEvtSub:Subscription;
  clearKeyboardEvtSub:Subscription;

  constructor(private utils:UtilsService) { 
    
    this.displayChordEvtSub = this.utils.getDisplayChordEvt().subscribe((chordName)=>{
      this.displayChord(chordName);
      })

    this.clearKeyboardEvtSub = this.utils.getClearKeayboardEvt().subscribe(()=>{
      this.clearKeyboard();
      })
  }

  ngOnInit(): void {
  }

  displayChord(chordName) {
    this.clearKeyboard();
    for (var key of this.utils.getChordKeysPf(chordName)) {
      this.highlightKey(key);
    }
  }

  highlightKey(key) {
    this.Keyboard.find(i => i.name === key).highlighted = true;
  }

  clearKeyboard() {
    this.Keyboard.forEach(key => {
      key.highlighted = false;
    });
  }

  toFrenchNotation(note) {
    return(this.utils.toFrenchNotation(note));
  }

}
