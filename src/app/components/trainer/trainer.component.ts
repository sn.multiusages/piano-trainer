import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  //trainingPeriod = 3; // seconds
  

  constructor(private utils: UtilsService) { }

  ngOnInit(): void {
  }

  curChordName = '...';
  trainingInProgress = false;
  pause = false;
  timeBetweenChords = 5; // seconds
  timeNextChords = 2;

  minor = true;
  major = true;
  sharp = true;

toFrenchNotation(note) {
  return(this.utils.toFrenchNotation(note));
}

getChordNameList() {
  return this.utils.getChordNameList();
}

onSelectChord(chord) {
  this.utils.displayChord(chord);
}

onChangeTimeBetwenChord(time) {
  this.timeBetweenChords = time;
  if (this.timeBetweenChords < 1) {
    this.pause = true;
  }
}

sleep(ms = 0) {
  return new Promise(r => setTimeout(r, ms));
}

waitNext() {
  return new Promise(r => this.pause === false);
}

onClickNextBtn() {
  this.pause = false;
}
async onClickTrainingBtn() {
  if (this.trainingInProgress === true) {
    this.trainingInProgress = false;
    this.curChordName = '...';
    this.utils.clearKeayboard();
  } else {
    this.trainingInProgress = true;
  }
  while (this.trainingInProgress === true) {
    this.utils.clearKeayboard();
    
    this.curChordName = this.utils.getRandomChord().name;
    await this.sleep(this.timeBetweenChords * 1000);
    this.utils.displayChord(this.curChordName);
    await this.sleep(3000);
    /*
    if (this.timeBetweenChords < 1) {
      this.pause = true;
    }
    await this.waitNext();
    */
  }
}
}
